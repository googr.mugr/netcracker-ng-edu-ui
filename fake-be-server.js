var express = require('express');
var bodyParser = require('body-parser');

var app = express();
app.set('port', (process.env.PORT || 8085));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use('/api/devices', express.static(__dirname + '/mocks/devices.json'));
app.use('/api/device', express.static(__dirname + '/mocks/device.json'));

app.listen(app.get('port'), function () {
  console.log('Fake Back-End Server listening on port ' + app.get('port'));
});
