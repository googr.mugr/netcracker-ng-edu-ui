import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {AppService} from './services/app.service';
import {HttpClientModule} from '@angular/common/http';
import {AlertModule} from 'ngx-bootstrap';
import {RouterModule, Routes} from '@angular/router';
import {DevicesComponent} from './components/devices/devices.component';
import {HomeComponent} from './components/home/home.component';
import { DeviceTypeIconComponent } from './components/device-type-icon/device-type-icon.component';
import {DeviceDetailsComponent} from "./components/device-details/device-details.component";
import {FormsModule} from "@angular/forms";

const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'devices', component: DevicesComponent},
  {path: 'home', component: HomeComponent},
  {path: 'device/:id', component: DeviceDetailsComponent},
];

@NgModule({
  declarations: [
    DeviceDetailsComponent,
    AppComponent,
    DevicesComponent,
    HomeComponent,
    DeviceTypeIconComponent
  ],
  imports: [
    FormsModule,
    HttpClientModule,
    BrowserModule,
    AlertModule.forRoot(),
    RouterModule.forRoot(routes)
  ],
  providers: [AppService],
  bootstrap: [AppComponent]
})
export class AppModule { }
