import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-device-type-icon',
  templateUrl: './device-type-icon.component.html',
  styleUrls: ['./device-type-icon.component.css']
})
export class DeviceTypeIconComponent {

  @Input('type') _type: string;

  constructor() {
  }
}
