import {Component, OnInit} from '@angular/core';
import {AppService} from '../../services/app.service';
import {Router} from '@angular/router';
import {Device} from "../../model/device";


@Component({
  selector: 'app-devices',
  templateUrl: './devices.component.html',
  styleUrls: ['./devices.component.css']
})
export class DevicesComponent implements OnInit {
  _devices: Device[] = [];
  _loading = true;

  constructor(private service: AppService,
              private router: Router) {
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.service.getDevices().subscribe((res: any) => {
      this._devices = res;
      this._loading = false;
    });
  }

  _showDetails(device) {
    this.router.navigate(['/device/' + device.id]);
  }
}
