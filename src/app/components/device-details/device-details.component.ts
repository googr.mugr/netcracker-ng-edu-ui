import {Component, OnInit} from '@angular/core';
import {AppService} from "../../services/app.service";
import {Device} from "../../model/device";

@Component({
  selector: 'app-device-details',
  templateUrl: './device-details.component.html',
  styleUrls: ['./device-details.component.css']
})
export class DeviceDetailsComponent implements OnInit {

  _device: Device;
  _loading = true;
  _showPopup = false;

  constructor(private service: AppService) {
  }

  ngOnInit() {
    this.getData();
  }

  public _saveDevice(name: string, prop1: string) {
    this._device.name = name;
    this._device.property1 = prop1;
  }

  private getData() {
    this.service.getDevice('todo').subscribe((res: any) => {
      this._device = res;
      this._loading = false;
    });
  }
}
