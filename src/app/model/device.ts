export class Device {
  id: number;
  name: string;
  type: string;
  property1: string;
  property2: string;
  property3: string;
}
