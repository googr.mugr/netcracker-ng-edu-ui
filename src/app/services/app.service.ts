import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private http: HttpClient) {
  }

  // baseUrl:string = "/assets/";
  readonly baseUrl = "/api";

  getDevices() {
    return this.http.get(this.baseUrl + "/devices");
  }

  getDevice(deviceId: string) {
    return this.http.get(this.baseUrl + "/device?id=" + deviceId);
  }

}
