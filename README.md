# minsk-ui-training
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.3.

## how start our project ?
install Node <https://nodejs.org/en/>

install angular CLI `npm install -g @angular/cli`

create new cli project `ng new my-project`

go `cd my-project`

install node dependency `npm i`

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

Run `npm run dev` for a dev server with proxy to BE.  Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

run `node fake-be-server.js` for a fake back-end server
## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.


